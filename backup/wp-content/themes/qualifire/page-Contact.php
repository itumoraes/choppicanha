<?php
/**
 * @package WordPress
 * @subpackage QualiFire
 */
/**
 * Template Name: Contact page
 */

if ( $qualifire_options['recaptcha_enabled'] == 'yes' ) {
    require_once('scripts/recaptcha/recaptchalib.php');
    $publickey = $qualifire_options['recaptcha_publickey']; // you got this from the signup page
    $privatekey = $qualifire_options['recaptcha_privatekey']; // you got this from the signup page
    $resp = null;
    $error = null;
    if( isset($_POST['submit']) ) {
	$resp = recaptcha_check_answer ($privatekey,
		    $_SERVER["REMOTE_ADDR"],
		    $_POST["recaptcha_challenge_field"],
		    $_POST["recaptcha_response_field"]
		);
	if ( !$resp->is_valid ) {
	    $rCaptcha_error = $resp->error;
	}
    }
}



get_header();

$content_position = ( $qualifire_options['contact_sidebar'] == 'left' ) ? 'grid_16 push_8' : 'grid_16';
$NA_phone_format = $qualifire_options['NA_phone_format'] ? '_NA_format' : '';

//If the form is submitted
if( isset($_POST['submit']) ) {
    // Get form vars
    $contact_name = trim(stripslashes($_POST['contact_name']));
    $contact_email = trim($_POST['contact_email']);
    $contact_phone = trim($_POST["contact_phone{$NA_phone_format}"]);
    $contact_ext = trim($_POST["contact_ext{$NA_phone_format}"]);
    $contact_message = trim(stripslashes($_POST['contact_message']));

    // Error checking if JS is turned off
    if( $contact_name == '' ) { //Check to make sure that the name field is not empty
	$nameError = __('Please enter a name', 'qualifire');
    } else if( strlen($contact_name) < 2 ) {
	$nameError = __('Your name must consist of at least 2 characters', 'qualifire');
    }

    if( $contact_email == '' ) {
	$emailError = __('Please enter a valid email address', 'qualifire');
    } else if( !is_email( $contact_email ) ) {
	$emailError = __('Please enter a valid email address', 'qualifire');
    }

    if( $NA_phone_format ) {
	if( !isPhoneNumberValid( $contact_phone ) || ( $contact_phone == '' && $contact_ext != '' ) ) {
	    $phoneError = __('phone number', 'qualifire');
	}
	if( !eregi("^[0-9]{0,5}$", $contact_ext) ) { // check if the extension consists of 1 to 5 digits, or empty
	    $extError = __('extension', 'qualifire');
	}
    }
    if( isset($phoneError) && isset($extError) ) {
	$phone_extError = sprintf(__('Please enter a valid %1$s and %2$s', 'qualifire'), $phoneError, $extError );
    } else if( isset($phoneError) ) {
	$phone_extError = sprintf(__('Please enter a valid %s', 'qualifire'), $phoneError );
    } else if( isset($extError) ) {
	$phone_extError = sprintf(__('Please enter a valid %s', 'qualifire'), $extError );
    }

    if( $contact_message == '' ) {
	$messageError = __('Please enter your message', 'qualifire');
    }

    if( !isset($nameError) && !isset($emailError) && !isset($messageError) && !isset($rCaptcha_error) ) {
	$ext = ( $contact_ext != '' ) ? ' ext.'.$contact_ext : '';
	$phone = ( $contact_phone != '' ) ? 'Phone: '.$contact_phone.$ext."\r\n" : '';
	// Send email
	$email_address_to = $qualifire_options['email_receipients'];
	$subject = sprintf(__('Contact Form submission from %s', 'qualifire'), get_option('blogname') );
	$message_contents = __("Sender's name: ", 'qualifire') . $contact_name . "\r\n" .
			    __('E-mail: ', 'qualifire') . $contact_email . "\r\n" .
			    __('Phone: ', 'qualifire') . $phone ."\r\n" .
			    __('Message: ', 'qualifire') . $contact_message . " \r\n";

	$header = "From: $contact_name <".$contact_email.">\r\n";
	$header .= "Reply-To: $contact_email\r\n";
	$header .= "Return-Path: $contact_email\r\n";
	$emailSent = ( @mail( $email_address_to, $subject, $message_contents, $header ) ) ? true : false;

	$contact_name_thx = $contact_name;

	// Clear the form
	$contact_name = $contact_email = $contact_phone = $contact_ext = $contact_message = '';
    }
}


//Contact Information Fields from the Admin Options
$contact_field_array = array(
    array(
	'desc' => $qualifire_options['contact_field_name1'],
	'value' => $qualifire_options['contact_field_value1'] ),
    array(
	'desc' => $qualifire_options['contact_field_name2'],
	'value' => $qualifire_options['contact_field_value2'] ),
    array(
	'desc' => $qualifire_options['contact_field_name3'],
	'value' => $qualifire_options['contact_field_value3'] ),
    array(
	'desc' => $qualifire_options['contact_field_name4'],
	'value' => $qualifire_options['contact_field_value4'] ),
    array(
	'desc' => $qualifire_options['contact_field_name5'],
	'value' => $qualifire_options['contact_field_value5'] ),
    array(
	'desc' => $qualifire_options['contact_field_name6'],
	'value' => $qualifire_options['contact_field_value6'] ),
    array(
	'desc' => $qualifire_options['contact_field_name7'],
	'value' => $qualifire_options['contact_field_value7']
    )
);


?>

<div id="content-container" class="container_24">
    <div id="main-content" class="<?php echo $content_position; ?>">
	<div class="main-content-padding">
<?php	if (have_posts()) : while (have_posts()) : the_post();
	  if($post->post_content != "") : ?>
	    <div class="post" id="post-<?php the_ID(); ?>">
		<div class="entry">
		    <?php the_content(__('<p class="serif">Read the rest of this page &raquo;</p>', 'qualifire')); ?>
		    <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
		</div>
	    </div>
<?php	  endif;
	endwhile; endif; ?>
	<?php edit_post_link(esc_html__('Edit this entry.', 'qualifire'), '<p class="editLink">', '</p>'); ?>
	<br />

	<div class="clear"></div>

<?php	// Contact Fields...
	if ( $qualifire_options['show_contact_fields'] ) : ?>
	    <div id="contactInfo">
<?php		foreach( $contact_field_array as $field_array ) :
		    if( $field_array['value'] != '' ) : ?>
			<div class="grid_4 contactFieldDesc"><?php echo $field_array['desc']; ?></div>
			<div class="grid_11 contactFieldValue"><?php echo $field_array['value']; ?></div>
			<div class="clear"></div>
<?php		    endif;
		endforeach; ?>
	    </div>
	    <div class="clear"></div>
<?php	endif; ?>

	<div id="contact-wrapper">
<?php	    // Message Area.  It shows a message upon successful email submission
	    if( isset( $emailSent ) && $emailSent == true ) : ?>
		<div class="success">
		    <div class="msg-box-icon">
			<strong><?php esc_html_e('Email enviado com sucesso!', 'qualifire'); ?></strong><br />
			<?php printf(__('Obrigado <strong>%s</strong> Seu e-mail foi enviado com sucesso e entraremos em contato em breve.', 'qualifire'), $contact_name_thx) ?>
		    </div>
		</div>
<?php	    elseif ( isset( $emailSent ) && $emailSent == false ) : ?>
		<div class="erroneous">
		    <div class="msg-box-icon">
			<?php esc_html_e('Falha ao conectar ao servidor de correio!', 'qualifire'); ?>
		    </div>
		</div>
<?php	    endif; ?>

	    <form id="contactForm" class="cmxform" method="post" action="<?php echo the_permalink(); ?>#contact-wrapper">
		<strong><?php esc_html_e('Utilize o formulario abaixo para nos enviar um e-mail:', 'qualifire'); ?></strong>
		<div>
		    <label for="contact_name"><?php esc_html_e('Nome', 'qualifire'); ?> </label><em><?php esc_html_e('(Obrigatorio)', 'qualifire'); ?></em><br />
		    <input id="contact_name" name="contact_name" size="30" class="required<?php if(isset($nameError)) echo ' error'; ?>" minlength="2" value="<?php echo esc_attr($contact_name); ?>" />
<?php		    if(isset($nameError)) echo '<label class="error" for="contact_name" generated="true">'.$nameError.'</label>'; ?>
		</div>
		<div>
		    <label for="contact_email"><?php esc_html_e('E-Mail', 'qualifire'); ?> </label><em><?php esc_html_e('(Obrigatorio)', 'qualifire'); ?></em><br />
		    <input id="contact_email" name="contact_email" size="30"  class="required email<?php if(isset($emailError)) echo ' error'; ?>" value="<?php echo esc_attr($contact_email); ?>" />
<?php		    if(isset($emailError)) echo '<label class="error" for="contact_email" generated="true">'.$emailError.'</label>'; ?>
		</div>
		<div>
		    <label for="contact_phone"><?php esc_html_e('telefone', 'qualifire'); ?> </label><em><?php esc_html_e('(opcional)', 'qualifire'); ?></em><br />
		    <input id="contact_phone<?php echo $NA_phone_format; ?>" name="contact_phone<?php echo $NA_phone_format; ?>" size="14" class="phone<?php if(isset($phoneError)) echo ' error'; ?>" value="<?php echo esc_attr($contact_phone); ?>" maxlength="14" />
		    <label for="contact_ext"><?php esc_html_e('ext.', 'qualifire'); ?> </label>
		    <input id="contact_ext<?php echo $NA_phone_format; ?>" name="contact_ext<?php echo $NA_phone_format; ?>" size="5" class="ext<?php if(isset($extError)) echo ' error'; ?>" value="<?php echo esc_attr($contact_ext); ?>" maxlength="5" />
<?php		    if(isset($phone_extError)) echo '<label class="error" for="contact_phone" generated="true">'.$phone_extError.'</label>'; ?>
		</div>
		<div>
		    <label for="contact_message"><?php esc_html_e('Mensagem', 'qualifire'); ?> </label><em><?php esc_html_e('(Obrigatorio)', 'qualifire'); ?></em><br />
		    <textarea id="contact_message" name="contact_message" cols="70" rows="7" class="required<?php if(isset($messageError)) echo ' error'; ?>"><?php echo esc_attr($contact_message); ?></textarea>
<?php		    if(isset($messageError)) echo '<br /><label class="error" for="contact_message" generated="true">'.$messageError.'</label>'; ?>
		</div>

<?php		if ( $qualifire_options['recaptcha_enabled'] == 'yes' ) : ?>
		    <script type="text/javascript">var RecaptchaOptions = {theme : '<?php echo $qualifire_options['recaptcha_theme']; ?>', lang : '<?php echo $qualifire_options['recaptcha_lang']; ?>'};</script>
		    <div>
<?php			echo recaptcha_get_html( $publickey, $rCaptcha_error ); ?>
		    </div>
<?php		endif; ?>

		<div>
		    <input name="submit" class="submit" type="submit" value="<?php esc_attr_e('Submit'); ?>"/>
		</div>
	    </form>

	</div><!-- end contact-wrapper -->
	</div><!-- end main-content-padding -->
    </div><!-- end main-content -->


<?php if( sidebar_exist('ContactSidebar') ) { get_sidebar('ContactSidebar'); } ?>

</div><!-- end content-container -->

<div class="clear"></div>

<?php

get_footer();




