<?php
/**
 * @package WordPress
 * @subpackage QualiFire
 */

global $qualifire_options, $style, $current_slider;
// get the current color scheme subdirectory
$style = ( $qualifire_options['color_scheme'] ) ? "style{$qualifire_options['color_scheme']}": "style1";


$logo_img_url = ( $qualifire_options['custom_logo_img'] ) ? $qualifire_options['custom_logo_img'] : get_bloginfo('template_url').'/styles/'.$style.'/images/logo.png';
$portfolio_cat_ID = $qualifire_options['portfolio_cat'];
$logo_width = $qualifire_options['logo_width'];
$logo_height = $qualifire_options['logo_height'];
$current_slider = $qualifire_options['current_slider'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

<!--[if IE 6]>
    <script  type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/DD_belatedPNG_0.0.8a-min.js"></script>
    <script  type="text/javascript">
    // <![CDATA[
	DD_belatedPNG.fix('.pngfix, img, #home-page-content li, #page-content li, #bottom li, #footer li, #recentcomments li span');
    // ]]>
    </script>
<![endif]-->

<?php wp_head(); ?>
<!--[if lte IE 8]>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/styles/common-css/ie-all.css" media="screen" type="text/css" />
<![endif]-->
<!--[if lte IE 7]>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/styles/common-css/ie6-7.css" media="screen" type="text/css" />
<![endif]-->
<!--[if IE 6]>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/styles/common-css/ie6.css" media="screen" type="text/css" />
    <style type="text/css">
	body{ behavior: url("<?php bloginfo('template_directory'); ?>/scripts/csshover3.htc"); }
    </style>
<![endif]-->

</head>
<body <?php body_class(); ?>>
    <div id="wrapper-1" class="pngfix">
	<div id="top-container" class="container_24">
	    <div class="clear"></div>
	    <div id="top" class="grid_24">
		<div id="logo" class="grid_14">
		    <h1>
			<a class="pngfix" style="background: transparent url( <?php echo esc_url($logo_img_url); ?> ) no-repeat 0 100%; width:<?php echo $logo_width; ?>px; height:<?php echo $logo_height; ?>px;" href="<?php echo get_option('home'); ?>">
			    <?php bloginfo('name'); ?>
			</a>
		    </h1>
		</div>
		<div id="slogan" class="grid_17" style="top:<?php echo $qualifire_options['slogan_distance_from_the_top']; ?>px; left:<?php echo $qualifire_options['slogan_distance_from_the_left']; ?>px;"><?php bloginfo('description'); ?></div>
		<!-- end logo slogan -->
		<div id="search" class="grid_7 prefix_17">
		    <form action="<?php bloginfo('home'); ?>/" method="get">
			<div class="search_box">
			    <input id="search_field" name="s" type="text" class="inputbox_focus inputbox pngfix" value="<?php esc_attr_e('Search...', 'qualifire'); ?>" />
			    <input type="submit"  value="" class="search-btn pngfix" />
			</div>
		    </form>
		</div><!-- end search -->
		<div class="phone-number grid_7 prefix_17">
<?php		    echo $qualifire_options['top_page_phone_number']; ?>
		</div>
		<!-- end top-icons -->
	    </div>
	    <!-- end top -->

	    <div class="clear"></div>

	    <div id="dropdown-holder" class="grid_24">
		<div class="nav_bg pngfix">
<?php		    qualifire_nav(); // this function calls the main menu ?>
		    <div class="nav-extra">
<?php			if( $qualifire_options['show_login_link_in_menu'] ) : ?>
			    <div class="nav-login"><?php wp_loginout(); ?></div>
<?php			endif; ?>
<?php			if( $qualifire_options['show_rss_link_in_menu'] ) : ?>
			    <div class="nav-rss">
				<a href="<?php bloginfo('rss2_url'); ?>" title="<?php esc_attr_e('Entries (RSS)', 'qualifire'); ?>">
				    <img src="<?php bloginfo('template_url'); ?>/styles/common-images/rss_16.png" alt="RSS" border="0" width="16" height="16" />
				</a>
			    </div>
<?php			endif; ?>
		    </div>
		</div>
	    </div><!-- end dropdown-holder -->
	</div>
	<!-- end top-container -->

	<div class="clear"></div>

<?php	if(is_front_page()) : ?>

<?php
	    if( $current_slider == '1' ) :
		include( 'sliders/piecemaker/piecemaker_display.php' );
	    elseif ( $current_slider == '2' ) :
		include( 'sliders/cycle/cycle1/cycle1_display.php' );
	    elseif ( $current_slider == '3' ) :
		include( 'sliders/cycle/cycle2/cycle2_display.php' );
	    elseif ( $current_slider == '4' ) : // no slider ?>
		<div id="page-content">
		    <div id="page-content-header" class="container_24">
			<div id="page-title">
			    <h2><?php echo $qualifire_options['no_slider_text']; ?></h2>
			</div>
			<div style="height:50px"> </div>
		    </div>
<?php	    endif; ?>

	    <div class="clear"></div>
<?php
	    if( $current_slider != '4' ) // no slider
		echo '<div id="home-page-content">' ?>
	    
<?php	else : ?>

	    <div id="page-content">
		<div id="page-content-header" class="container_24">
		    <div id="page-title">
<?php 			$post = $posts[0]; // Hack. Set $post so that the_date() works.
			if (is_page()) : ?>
			    <h2><?php the_title(); ?></h2>
<?php			elseif ( is_single() ) : ?>
			    <h2><?php the_category(', '); ?></h2>
<?php			elseif (is_category()) : /* If this is a category archive */ ?>
			    <h2 class="pagetitle"><?php printf( __('Archive for the &#8216;%s&#8217; Category', 'qualifire' ), single_cat_title("", false) ); ?></h2>
<?php			elseif (is_search()) : /* If this is a search results page */ ?>
			    <h2 class="pagetitle"><?php printf( __('Search Results for &#8216;<em>%s</em>&#8216;', 'qualifire' ), get_search_query() ); ?></h2>
<?php			elseif (is_404()) : /* If this is a search results page */ ?>
			    <h2 class="pagetitle"><?php esc_html_e('Page Not Found (Error 404)', 'qualifire'); ?></h2>
<?php			elseif( is_tag() ) : /* If this is a tag archive */ ?>
			    <h2 class="pagetitle"><?php printf( __('Posts Tagged &#8216;%s&#8217;', 'qualifire' ), single_tag_title("", false) ); ?></h2>
<?php			elseif (is_day()) : /* If this is a daily archive */ ?>
			    <h2 class="pagetitle"><?php printf( __('Archive for %s', 'qualifire' ), get_the_time('F jS, Y') ); ?></h2>
<?php			elseif (is_month()) : /* If this is a monthly archive */ ?>
			    <h2 class="pagetitle"><?php printf( __('Archive for %s', 'qualifire' ), get_the_time('F, Y') ); ?></h2>
<?php			elseif (is_year()) : /* If this is a yearly archive */ ?>
			    <h2 class="pagetitle"><?php printf( __('Archive for %s', 'qualifire' ), get_the_time('Y') ); ?></h2>
<?php			elseif (is_author()) : /* If this is an author archive */ ?>
			    <h2 class="pagetitle"><?php esc_html_e('Author Archive', 'qualifire'); ?></h2>
<?php			elseif (isset($_GET['paged']) && !empty($_GET['paged'])) : /* If this is a paged archive */ ?>
			    <h2 class="pagetitle"><?php esc_html_e('Blog Archives', 'qualifire'); ?></h2>
<?php			endif; ?>
		    </div>

<?php		    // Breadcrumbs
		    if ( ($qualifire_options['show_breadcrumbs'] == 'yes') && class_exists('simple_breadcrumb') ) :
			$breadcrumbs_go = new simple_breadcrumb;
		    else : ?>
			<div class="no-breadcrumbs-padding"></div>
<?php		    endif; ?>


		</div>
	    
<?php	endif; ?>




