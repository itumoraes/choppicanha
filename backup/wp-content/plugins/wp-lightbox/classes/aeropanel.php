<?PHP

if ( !class_exists("plugin_aeropanel") )
{
  class plugin_aeropanel
  {
    var $plugin  = array();
    var $version = "1.1.2";
    var $styles  = array();
    var $scripts = array();
    
    function plugin_aeropanel($plugin='')
    {
      $this->plugin = (array)$plugin;
      
      if (!get_option($this->plugin['shortname']."_installed"))
      {
        foreach ($this->get_options($this->plugin['options']) as $value)
        {
          $type = $value['type'];
          if ($type=="text" || $type=="textarea" || $type=="select" || $type=="checkbox" )
          {
            add_option($this->plugin['shortname'].'_'.$value['id'], $value['default']);
          }
        }
        add_option($this->plugin['shortname'].'_installed', true);
      }

      add_action('admin_init', array($this, 'admin_init'));
      add_action('admin_menu', array($this, 'admin_menu'));
      if ($_REQUEST['action']!='') $this->save();
    }

    function path($file='')
    {
      if ($file == '')
      {
        $file = $this->plugin['file'];
      }
      if ($file === true)
      {
        return plugin_dir_path($this->plugin['file']);
      }
      else
      {
        return plugins_url(plugin_basename(dirname($file)));
      }
    }

    function admin_init()
    {
      wp_register_style("aeropanel", $this->path() . "/css/style.css", false, $this->plugin['Version'], "all");
      wp_register_script("aeropanel", $this->path() . "/js/aeropanel/panel.js", false, $this->plugin['Version']);
    }

    function admin_styles()
    {
      wp_enqueue_style('aeropanel');
      wp_enqueue_script('aeropanel');

      foreach((array)$this->styles as $style)
      {
        wp_enqueue_style(basename($style), $style, false, $this->plugin['Version'], "all");
      }

      foreach((array)$this->scripts as $script)
      {
        wp_enqueue_script(basename($script), $script, false, $this->plugin['Version'], "all");
      }
    }
    
    function queue($file, $type)
    {
      if ($type == 'style')
      {
        $this->styles[] = $file;
      }
      else if ($type == 'script')
      {
        $this->scripts[] = $file;
      }
    }
    
    function save_option($option, $value)
    {
      return update_option($this->plugin['shortname']."_$option", $value);
    }
    
    function get_option($option)
    {
      return get_option($this->plugin['shortname']."_$option");
    }
    
    function get_options($array, &$out = array())
    {
      foreach($array as $key => $child)
      {
        if(is_array($child))
        {
          if ($child['type'] != '' && $child['id'] != '')
          {
            $out[] = $child;
          }
          else
          {
            $out = $this->get_options($child, $out);
          }
        }
      }
      return $out;
    }

    /*!
     * Add a new submenu under Settings
     */
    function admin_menu()
    {
      if ( current_user_can('manage_options') )
      {
        $this->plugin = array_merge($this->plugin, get_plugin_data($this->plugin['file'], false, false));
      }
      if (function_exists('add_options_page'))
      {
        $page = add_options_page($this->plugin['Name'], $this->plugin['Title'], 8, basename($this->plugin['file']), array($this, 'admin'));
        add_action('admin_print_styles-' . $page, array($this, 'admin_styles'));
      }
    }

    /*!
     * Show the panel
     */  
    function admin()
    {
      $i=0;
      
      if ( $_REQUEST['action'] == 'save' ) echo '<div id="message" class="updated fade"><p><strong>'.$this->plugin['Title'].' settings saved.</strong></p></div>';
      if ( $_REQUEST['action'] == 'reset') echo '<div id="message" class="updated fade"><p><strong>'.$this->plugin['Title'].' settings reset.</strong></p></div>';
   
      include('buildpage.php');
    }
    
    /*!
     * Save options, and reset options
     */
    function save()
    {
      if ( $_GET['page'] == basename($this->plugin['file']) )
      {
        if ( 'save' == $_REQUEST['action'] )
        {
          foreach ($this->get_options($this->plugin['options']) as $value)
          {
            $value['id'] = $this->plugin['shortname'].'_'.$value['id'];
            if ( isset($_REQUEST[$value['id']]) )
            {
              update_option( $value['id'], $_REQUEST[ $value['id'] ]  );
            }
            else
            {
              delete_option( $value['id'] );
            }
          }
        }
        else if ( 'reset' == $_REQUEST['action'] )
        {
          foreach ($this->get_options($this->plugin['options']) as $value)
          {
            delete_option( $this->plugin['shortname'].'_'.$value['id'] );
          }
          
          delete_option( $this->plugin['shortname']."_installed" );
        }
      }
    }

  }
}

?>