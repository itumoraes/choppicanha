<?php if ( show_webapp_notice() ) { ?>
	<div id="web-app-overlay">
		<a href="#" id="close-wa-overlay">X</a>
		<img src="<?php  echo wptouch_get_site_menu_icon( WPTOUCH_ICON_BOOKMARK ); ?>" alt="bookmark-icon" />
		<h2><?php wptouch_bloginfo( 'site_title' ); ?></h2>
		<h3><?php _e( "is now web-app enabled!", "wptouch-pro" ); ?></h3>
		<p><?php _e( "Save", "wptouch-pro" ); ?> <?php wptouch_bloginfo( 'site_title' ); ?> <?php _e( "as a web-app on your Home Screen.", "wptouch-pro" ); ?></p>
		<p><?php _e( "Tap + then Add to Home Screen.", "wptouch-pro" ); ?></p>
	</div>
<?php } ?>