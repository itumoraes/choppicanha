<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'choppicanha');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '30seconds');

/** nome do host do MySQL */
define('DB_HOST', 'localhost:8888');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'FFv!Zqym!;m+a.GilMl]_m3X@W/N;a%c<)3[3-2(7SvcLmT[ZOIs-MB{Km{Mj|Eo');
define('SECURE_AUTH_KEY',  '* Nj,ces/mh9Z$w26-!#?[O$r4W7t~F ,hZ!7$dLgHW[-Y|n+PQ*eCz+^mk,L<+a');
define('LOGGED_IN_KEY',    'fAv!;N KwoK.Dz0*J}uDV!&0F3a?K}/dH+7VoOH|,DE%~=9s|(F(|.C27`Op-HdX');
define('NONCE_KEY',        'juv.kN%ndbAKnJ|*LOMt==gJCO&T(2<u#Ao$?6vJ?PN)Lh88Y[DhE`&CPBXEy2+?');
define('AUTH_SALT',        'QNo}LG>B]T<*03OO5G0i.hikIVwptz6Lo2PQO(~?|Z$xf3e;[`#-.zU-+=?JP8B*');
define('SECURE_AUTH_SALT', ':}q6&A.v<}3.!VbO7sMVD7i%{0,hX`f_En9+Uha;rx2{[ W -3e5xiq!-2u:y9,9');
define('LOGGED_IN_SALT',   't~#eGCi1(K|0_B)d%(k,$_@.--b1:{nU1e2UHt5GwQL <Qqo3B[T(=b&H7 v*v(l');
define('NONCE_SALT',       'L$a0o:] -8$>[jJcbl-UY|p2>g$g&=p[-#&@1I8-`im(d^B{AL|/OEEpWw2%0+rM');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * O idioma localizado do WordPress é o inglês por padrão.
 *
 * Altere esta definição para localizar o WordPress. Um arquivo MO correspondente ao
 * idioma escolhido deve ser instalado em wp-content/languages. Por exemplo, instale
 * pt_BR.mo em wp-content/languages e altere WPLANG para 'pt_BR' para habilitar o suporte
 * ao português do Brasil.
 */
define('WPLANG', 'pt_BR');

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
