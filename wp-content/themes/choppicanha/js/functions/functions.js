﻿$(document).ready(function () {
	menuSize();
	makeForm();
	
	$("#message").css("left", ($(window).width() - $("#message").width()) / 2);
	
	var cont = 0;
	
	$(".gallery").each(function() {
		cont++;
		$(this).find("a").each(function() { 
			$(this).attr({
				"class": "fancybox",
				"rel": "galeria" + cont
			});
		});
	});
	
	$(".fancybox").fancybox({
		openEffect	: 'fade',
		closeEffect	: 'fade'
	});
});

$(window).resize(function() {
	menuSize();
	
	$("#message").css("left", ($(window).width() - $("#message").width()) / 2);
});

/*
$(window).scroll(function() {
	var position = $("#top").offset().top - $(window).scrollTop();
	
	if(position <= 0) {
		$("#top").css({
			"position": "fixed",
			"top": "0"
		});
	} else {
		$("#top").css({
			"position": "relative",
			"top": "initial"
		});
	}
});
*/

//Função que calcula o tamanho dos menus
function menuSize() {
	$("#menu ul").each(function() {
		$(this).width(($(this).closest(".columns").width() - 278) / 2);
	});

};

//Função que cria os atributos de input e os fakes submits de um form
function makeForm() {
	function setValue(e) {
		$(e).attr("data-placeholder", $(e).val());
		$(e).css("color", "#dfdfdf");
		
		$(e).focus(function() {
			if($.trim($(e).val().toLowerCase()) == $.trim($(e).attr("data-placeholder").toLowerCase()) || $(e).val() == "") {
				$(e).css("color", "#303030");
				$(e).val("");
			}
		}).blur(function() {
			if($.trim($(e).val().toLowerCase()) == $.trim($(e).attr("data-placeholder").toLowerCase()) || $(e).val() == "") {
				$(e).css("color", "#dfdfdf");
				$(e).val($(e).attr("data-placeholder"));
			}
		});
	}

	$("form").each(function() {
		$(this).find("input[type=submit]").before("<a class='fake-submit button red-button' onclick='validaForm(this);'>" + $(this).find("input[type=submit]").attr("value") + "</a>");
		$(this).find("input[type=submit]").hide();
		$(this).find(".vfb-item-secret").remove();
	});
	
	$("input").each(function() {
		switch($(this).attr("type")) {
			case "text":
				setValue($(this));
				break;
			case "email":
				setValue($(this));
				break;
			case "search":
				setValue($(this));
				break;
		}
	});
	
	$("textarea").each(function() {
		setValue($(this));
	});
}

//Função que valida um formulário
function validaForm(obj) {
	var cont = 0;

	function validaValue(e) {
		if($.trim($(e).val().toLowerCase()) == $.trim($(e).attr("data-placeholder").toLowerCase()) || $(e).val() == "") {
			cont++;
			return cont;	
		} else {
			return cont;
		}
	}

	$(obj).closest("form").find("input").each(function() {
		switch($(this).attr("type")) {
			case "text":
				validaValue($(this));
				break;
			case "email":
				validaValue($(this));
				break;
			case "search":
				validaValue($(this));
				break;
		}
	});
	
	if($(obj).closest("form").attr("id") == "contato-1") {
		validaValue($(obj).closest("form").find("textarea"));
	}
	
	if(cont != 0) {
        $("#message").css({
            "visibility": "visible",
            "opacity": "1",
            "top": "10px"
        });
        
        setTimeout(function() {
	        $("#message").css({
	            "visibility": "hidden",
	            "opacity": "0",
	            "top": "0px"
	        });
        }, 5000);
	} else {
		$(obj).next().click();
	}
}

//Função que abre o select de Cardápio
function openSelect() {
	var s = $("#select");
	
	$(".subselect").each(function() {
		if($(this).attr("data-situation") == "active") {
			$(this).attr("data-situation", "disable");
			$(this).hide("slide", { direction: "up" },  400);
		}
	});
	
	$(".pratos").each(function() {
		if($(this).attr("data-situation") == "active") {
			$(this).attr("data-situation", "disable");
			$(this).hide("slide", { direction: "up" },  400);
			$(".select-title span").text("Clique e Escolha");
		}
	});
	
	switch(s.attr("data-situation")) {
		case "active":
			s.attr("data-situation", "disable");
			s.show("slide", { direction: "up" }, 400);
			break;
		case "disable":
			s.attr("data-situation", "active");
			s.hide("slide", { direction: "up" }, 400);
			break;
		case "pedding":
			s.attr("data-situation", "active");
			setTimeout(function() {
				s.show("slide", { direction: "up" }, 400);
			}, 400);
			break;
	}
}

//Função que ativa o select do Cardápio
function activeSelect(obj) {
	var s = $("#select");
	var sub = $(".subselect[data-select=" + $(obj).attr("data-cat") + "]");
	
	s.attr("data-situation", "pedding");
	s.hide("slide", { direction: "right" }, 400);
	
	setTimeout(function() {
		sub.attr("data-situation", "active");
		sub.show("slide", { direction: "up" }, 400);
	}, 400);
}

//Função que ativa o prato a ser visualizado no Cardápio
function activePratos(obj) {
	var pratos = $(".pratos[data-cat=" + $(obj).parent().attr("data-select") + "]");
	var sub = $(obj).parent();
	
	sub.attr("data-situation", "disable");
	sub.hide("slide", { direction: "right" }, 400);
	
	$(".select-title span").text($(obj).text());
	
	setTimeout(function() {
		pratos.attr("data-situation", "active");
		pratos.show("slide", { direction: "up" }, 400);
	}, 400);
	
}

//Função que apresenta o mapa inicial
function mapsInitialize() {
    google.maps.visualRefresh = true;

    var mapInitializeOptions = {
        zoom: 17,
        center: new google.maps.LatLng(-3.737648, -38.545573),
        scrollwheel: false,
        disableDefaultUI: true
    };

    var mapInitialize = new google.maps.Map(document.getElementById("map"), mapInitializeOptions);

    var contentInitializeString =   "<div class='maps'>" +
                                        "<h5>Choppicanha Parquelândia</h5>" +
                                        "<p>" + 
                                            "Avenida Jovita Feitosa, 191 <br/>" + 
                                            "Parquelândia - Fortaleza - Ceará <br/>" + 
                                            "<strong>Telefone: (85)3452 9199</strong>" + 
                                        "</p>" +
                                    "</div>";

    var infoInitializeWindow = new google.maps.InfoWindow({
        content: contentInitializeString
    });

    infoInitializeWindow.setOptions({
        maxWidth: 600
    });

    var markerInitialize = new google.maps.Marker({
        position: new google.maps.LatLng(-3.737648, -38.545573),
        map: mapInitialize,
        title: "Choppicanha"
    });

    google.maps.event.addListener(markerInitialize, "click", function () {
        infoInitializeWindow.open(mapInitialize, markerInitialize);
    });

    infoInitializeWindow.open(mapInitialize, markerInitialize);
};

//Função que faz a modificação do mapa apresentado na sessão de lojas
function googleMaps(x, y, obj) {
	google.maps.visualRefresh = true;

    var myLatlng = new google.maps.LatLng(x, y);

    var mapOptions = {
        zoom: 17,
        center: myLatlng,
        scrollwheel: false,
        disableDefaultUI: true
    };

    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    
    if($(obj).attr("id") == "parquelandia") {
	    var contentString =   "<div class='maps'>" +
                                    "<h5>Choppicanha Parquelândia</h5>" +
                                    "<p>" + 
                                        "Avenida Jovita Feitosa, 191 <br/>" + 
                                        "Parquelândia - Fortaleza - Ceará <br/>" + 
                                        "<strong>Telefone: (85)3452 9199</strong>" + 
                                    "</p>" +
                                "</div>";
    } else {
	    var contentString =   "<div class='maps'>" +
                                    "<h5>Choppicanha Monte Castelo</h5>" +
                                    "<p>" + 
                                        "Rua Benjamin Barroso, 745 <br/>" + 
                                        "Monte Castelo - Fortaleza - Ceará <br/>" + 
                                        "<strong>Telefone: (85)3452 9199</strong>" + 
                                    "</p>" +
                                "</div>";
    }

    var infoWindow = new google.maps.InfoWindow({
        content: contentString
    });

    infoWindow.setOptions({
        maxWidth: 600
    });

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: "Choppicanha"
    });

    google.maps.event.addListener(marker, "click", function () {
        infoWindow.open(map, marker);
    });

    infoWindow.open(map, marker);
    
    $("html, body").animate({scrollTop: $("html, body").offset().top}, 400);
};

function activeMenu() {
	if($(".menu-button").attr("data-situation") == "disable") {
		$(".menu-button").attr("data-situation", "active");
		$("#mobile-menu").css("margin-top", "0px");
	} else {
		$(".menu-button").attr("data-situation", "disable");
		$("#mobile-menu").css("margin-top", "-240px");
	}
}