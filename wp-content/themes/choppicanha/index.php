<?php get_header(); ?>

	<section id="hero">
	
		<div id="mosaic">
			<img src="<?php bloginfo("template_url"); ?>/images/mosaico.png" alt="Mosaico Choppicanha" />
		</div>
	
	</section>
	
	<?php include("page-templates/menu.php"); ?>
	
	<section id="place">
	
		<div class="row">
		
			<div class="large-12 medium-12 small-12 columns">
			
				<div class="title">
				
					<span></span>
					
					<h1>Espaço Choppicanha</h1>
				
				</div>
			
			</div>
			
			<div class="clearfix"></div>
			
			<ul>
				<li class="large-3 medium-3 small-6 columns"><a class="fancybox" href="<?php bloginfo("template_url"); ?>/images/fotos_01.jpg" style="background-image: url(<?php bloginfo("template_url"); ?>/images/fotos_01.jpg);"></a></li>
				<li class="large-3 medium-3 small-6 columns"><a class="fancybox" href="<?php bloginfo("template_url"); ?>/images/fotos_02.jpg" style="background-image: url(<?php bloginfo("template_url"); ?>/images/fotos_02.jpg);"></a></li>
				<li class="large-3 medium-3 small-6 columns"><a class="fancybox" href="<?php bloginfo("template_url"); ?>/images/fotos_03.jpg" style="background-image: url(<?php bloginfo("template_url"); ?>/images/fotos_03.jpg);"></a></li>
				<li class="large-3 medium-3 small-6 columns"><a class="fancybox" href="<?php bloginfo("template_url"); ?>/images/fotos_04.jpg" style="background-image: url(<?php bloginfo("template_url"); ?>/images/fotos_04.jpg);"></a></li>
			</ul>
			
			<div class="clearfix"></div>
			
			<div class="large-10 large-offset-1 medium-10 medium-offset-1 small-12 columns">
			
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo.</p>
				
			</div>
			
			<div class="clearfix"></div>
			
			<div class="large-4 large-offset-4 medium-6 medium-offset-3 small-12 columns">
			
				<a class="button red-button" href="<?php bloginfo("template_uri"); ?>/choppicanha">Conhecer o Espaço</a>
			
			</div>
			
			<div class="clearfix"></div>
		
		</div>
	
	</section>
	
	<section id="highlight">
	
		<div class="row">
		
			<?php
				$args = array( 'post_type' => 'destaquecardapio', 'showposts' => '1', 'order' => "ASC");
				$myposts = get_posts($args);
				
				foreach ( $myposts as $post ) : setup_postdata( $post );	
					
					?>
					
						<div class="large-6 medium-6 small-12 columns">
			
							<div class="image-container">
								
								<span class="seal"></span>
								
								<?php the_post_thumbnail($post->ID); ?>
							
							</div>
						
						</div>
					
						<div class="large-6 medium-6 small-12 columns">

							<div class="highlight-title">
							
								<div class="highlight-title">
								
											
									<h2>
										<span class="left-detail"></span>
										Destaque do Cardápio
										<span class="right-detail"></span>	
									</h2>
									
									
									<div class="clearfix"></div>
									
								</div>
								
							</div>
							
							<h4><?php the_title(); ?></h4>
							
							<span class="price"><?php the_field('preço'); ?></span>
							
							<div class="clearfix"></div>
							
							<?php the_content(); ?>
							
							<a href="<?php bloginfo("template_uri"); ?>/cardapio" class="button black-button">Ver Cardápio</a>
						
						</div>	
					
					<?php
										
				endforeach;
				
				wp_reset_postdata();
			?>
		
		</div>
	
	</section>
	
	<section id="delivery">
	
		<div class="row">
		
			<div class="large-12 columns">
	
				<img src="<?php bloginfo("template_url"); ?>/images/banner-delivery.jpg" alt="Delivery Choppicanha. Chega rápido e quentinho. (85) 3452 9199"/>
			
			</div>
		
		</div>
	
	</section>

<?php get_footer();?>
