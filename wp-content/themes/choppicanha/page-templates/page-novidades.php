<?php
	/* Template Name: Novidades */
	
	get_header(); 
?>

	<?php include("menu.php"); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<section id="novidades">
			
			<div class="row">
			
				<div class="large-12 medium-12 small-12 columns">
				
					<div class="title">
					
						<span></span>
						
						<h1><?php the_title(); ?></h1>
					
					</div>
				
				</div>
				
				<div class="clearfix"></div>
				
				<div class="large-9 medium-9 small-12 columns">
					
					<?php $args = array( 'post_type' => 'post', 'showposts' => '-1', 'order' => "ASC"); ?>

	                <?php $myposts = get_posts($args); ?>
	
	                <?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
	
	                    <div class="item">
	
	                        <div class="head">
	                        
	                        	<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
	
	                            <div class="date">
	                                <span>Postado em <?php echo get_the_date(); ?></span>
	                            </div>
	                            
	                            <div class="clearfix"></div>
	
	                        </div>
	                        
	                        <div class="image-box">
	                        	
	                        	<a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($page->ID, "full"); ?></a>
	                        	
	                        </div>
	
	                        <article class="textbox">
	
	                            <?php the_excerpt(); ?>
	
	                        </article>
	
	                    </div>
	
	                <?php endforeach; ?>
	                
	                <div class="navigation">
	                
	                	<?php twentyfourteen_paging_nav(); ?>
	                
	                </div>
	
	                <?php wp_reset_postdata();?>
										
				</div>
				
				<div class="large-3 medium-3 small-12 columns">
				
					<?php get_sidebar(); ?>
				
				</div>
			
			</div>
			
		</section>
	
	<?php endwhile; ?>

<?php get_footer(); ?>