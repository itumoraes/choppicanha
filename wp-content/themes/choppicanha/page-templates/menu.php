<section id="top">

	<div id="responsive-menu">
	
		<div class="row">
		
			<div class="large-12 medium-12 small-12 columns">
			
				<a href="<?php bloginfo("url"); ?>"><img id="negative-logo" src="<?php bloginfo("template_url"); ?>/layout/images/logo-choppicanha-negativa.png" alt="Logo Choppicanha Negativa"/></a>
			
				<a onclick="activeMenu();" class="menu-button" data-situation="disable">Menu</a>
				
				<div class="clearfix"></div>
			
			</div>
		
		</div>
	
	</div>
	
	<nav id="menu">
	
		<div class="row">
		
			<div class="large-12 medium-12 small-12 columns">
				
				<?php wp_nav_menu( array("menu" => "Menu Esquerda", "container_class" => "left-menu") ); ?>
				
				<a id="logo" href="<?php bloginfo("url"); ?>">Logo Choppicanha</a>
				
				<?php wp_nav_menu( array("menu" => "Menu Direita", "container_class" => "right-menu") ); ?>
				
				<div class="clearfix"></div>
			
			</div>
		
		</div>
	
	</nav>

</section>