<?php
	/* Template Name: Localização */
	
	get_header(); 
?>

	<?php include("menu.php"); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<section id="localizacao">
		
			<div id="map">
				
			</div>
			
			<div class="clearfix"></div>
			
			<div class="row">
			
				<div class="large-12 medium-12 small-12 columns">
				
					<div class="title">
					
						<span></span>
						
						<h1><?php the_title(); ?></h1>
					
					</div>
				
				</div>
				
				<div class="clearfix"></div>
				
				<div class="large-10 large-offset-1 medium-10 medium-offset-1 small-12 columns">
					
					<?php the_content(); ?>
					
				</div>
				
				<div class="clearfix"></div>
				
				<div class="large-5 large-offset-1 medium-6 small-12 columns">
				
					<div class="address-container">
					
						<h2>Loja Parquelândia</h2>
						
						<p>
							Avenida Jovita Feitosa, 191 <br/>
							Parquelândia - Fortaleza - Ceará <br/>
							(85)3452 9199
						</p>
						
						<a id="parquelandia" onclick="googleMaps(-3.737648, -38.545573, this);" class="button red-button">Abrir Mapa</a>
					
					</div>
				
				</div>
				
				<div class="large-5 large-pull-1 medium-6 small-12 columns">
				
					<div class="address-container">
					
						<h2>Loja Monte Castelo</h2>
						
						<p>
							Rua Benjamin Barroso, 745 <br/>
							Monte Castelo - Fortaleza - Ceará <br/>
							(85)3452 9199
						</p>
						
						<a id="montecastelo" onclick="googleMaps(-3.731265, -38.550450, this);" class="button red-button">Abrir Mapa</a>
					
					</div>
				
				</div>
			
			</div>
			
		</section>
		
		<script>
			$(document).ready(function() {
				mapsInitialize();
			});
		</script>
	
	<?php endwhile; ?>

<?php get_footer(); ?>