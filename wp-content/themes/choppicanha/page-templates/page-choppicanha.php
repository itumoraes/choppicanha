<?php
	/* Template Name: Choppicanha */
	
	get_header(); 
?>

	<?php include("menu.php"); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<section id="choppicanha">
		
			<div class="full-image" style="background-image: url(<?php the_field("imagem_choppicanha"); ?>);">
			</div>
			
			<div class="content row">
			
				<div class="large-12 medium-12 small-12 columns">
			
					<div class="title">
					
						<span></span>
						
						<h1><?php the_field("titulo_choppicanha"); ?></h1>
					
					</div>
				
				</div>
				
				<div class="clearfix"></div>
				
				<div class="large-10 large-offset-1 medium-10 medium-offset-1 small-12 columns">
					
					<?php the_field("texto_choppicanha"); ?>
					
				</div>
				
				<div class="clearfix"></div>
				
				<div class="large-5 large-offset-1 medium-6 small-12 columns">
				
					<div class="gallery-container">
					
						<h2>Loja Parquelândia</h2>
						
						<?php the_field("galeria_parquelandia"); ?>
						
						<div class="clearfix"></div>
					
					</div>
					
				</div>
				
				<div class="large-5 medium-6 small-12 columns">
				
					<div class="gallery-container">
					
						<h2>Loja Monte Castelo</h2>
						
						<?php the_field("galeria_monte_castelo"); ?>
						
						<div class="clearfix"></div>
					
					</div>
				
				</div>
				
				<div class="clearfix"></div>
			
			</div>
			
			<div class="full-image" style="background-image: url(<?php the_field("imagem_funcionamento"); ?>);">
			</div>
			
			<div class="content row">
				
				<div class="large-12 medium-12 small-12 columns">
				
					<div class="title">
					
						<span></span>
						
						<h1><?php the_field("titulo_funcionamento"); ?></h1>
					
					</div>
				
				</div>
				
				<div class="clearfix"></div>
				
				<div class="large-10 large-offset-1 medium-10 medium-offset-1 small-12 columns">
					
					<?php the_field("texto_funcionamento"); ?>
					
				</div>
				
				<div class="clearfix"></div>
				
			</div>
			
			<div class="full-image last-image" style="background-image: url(<?php the_field("imagem_nossa_estrutura"); ?>);">
			</div>
			
			<div class="content row">
				
				<div class="large-12 medium-12 small-12 columns">
				
					<div class="title">
					
						<span></span>
						
						<h1><?php the_field("titulo_nossa_estrutura"); ?></h1>
					
					</div>
				
				</div>
				
				<div class="clearfix"></div>
				
				<div class="large-10 large-offset-1 medium-10 medium-offset-1 small-12 columns">
					
					<?php the_field("texto_nossa_estrutura"); ?>
					
				</div>
				
				<div class="clearfix"></div>
				
			</div>
		
		</section>
	
	<?php endwhile; ?>

<?php get_footer(); ?>
