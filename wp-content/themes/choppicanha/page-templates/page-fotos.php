<?php
	/* Template Name: Fotos */
	
	get_header(); 
?>

	<?php include("menu.php"); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<section id="fotos">
			
			<div class="row">
			
				<div class="large-12 medium-12 small-12 columns">
				
					<div class="title">
					
						<span></span>
						
						<h1><?php the_title(); ?></h1>
					
					</div>
				
				</div>
				
				<div class="clearfix"></div>
				
				<div class="large-10 large-offset-1 medium-10 medium-offset-1 small-12 columns">
					
					<?php the_content(); ?>
					
				</div>
			
			</div>
			
		</section>
	
	<?php endwhile; ?>

<?php get_footer(); ?>