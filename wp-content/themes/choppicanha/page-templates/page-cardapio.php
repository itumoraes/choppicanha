<?php
	/* Template Name: Cardápio */
	
	get_header(); 
?>

	<?php include("menu.php"); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<section id="cardapio">
			
			<div class="row">
			
				<div class="large-12 medium-12 small-12 columns">
				
					<div class="title">
					
						<span></span>
						
						<h1><?php the_title(); ?></h1>
					
					</div>
					
					<?php the_content(); ?>
				
				</div>
				
				<div class="clearfix"></div>
				
				<div class="large-10 large-offset-1 medium-10 medium-offset-1 small-12 columns">
				
					<div class="cardapio-container">
					
						<div class="select-title" onclick="openSelect();">
							<span>Clique e Escolha</span>
						</div>
						
						<?php
							$categories = get_categories('child_of=53');
							
							echo "<div id='select' data-situation='active' style='display: none;'>";
							
								foreach ( $categories as $categorie ){
									echo "<a data-cat='".$categorie->slug."' onclick='activeSelect(this);'>".$categorie->cat_name."</a>";
									wp_reset_postdata();
								}
							
							echo "</div>";
						?>
						
						<?php
							$categories = get_categories('child_of=53');
							
								foreach ( $categories as $categorie ){
									echo "<div class='subselect' data-situation='disable' style='display: none;' data-select='".$categorie->slug."'>";
										
										$args = array( 'post_type' => 'cardapio', 'showposts' => '-1', 'order' => "ASC", 'category' => $categorie->cat_ID);
										$myposts = get_posts($args);
										
										foreach ( $myposts as $post ) : setup_postdata( $post );	
											
											echo "<a data-cat='".$post->post_name."' onclick='activePratos(this);'>".$post->post_title."</a>";
												
										endforeach;
										
										wp_reset_postdata();
									
									echo "</div>";
								}
						?>
		                
		                <div class="pratos-container">
		                
<!-- 		                	<span data-situation="active">Nenhuma categoria do cardápio selecionada</span> -->
		                	
		                	<?php
								$categories = get_categories('child_of=53');

								foreach ( $categories as $categorie ){
									echo "<ul class='pratos' style='display: none;' data-cat='".$categorie->slug."' data-situation='disable'>";
									
										$args = array( 'post_type' => 'cardapio', 'showposts' => '-1', 'order' => "ASC", 'category' => $categorie->cat_ID);
										$myposts = get_posts($args);
										
										foreach ( $myposts as $post ) : setup_postdata( $post );	
											
											$field_group_values = simple_fields_fieldgroup("pratos");
											
											foreach($field_group_values as $field) : setup_postdata( $post ); 
												echo "<li>";
													echo "<span class='prato-title'>".$field[nome]."</span>";
													echo "<span class='prato-price'>R$".$field[preco]."</span>";
												echo "</li>";
											endforeach;
												
										endforeach;
										
									echo "</ul>";
									
									wp_reset_postdata();
								}
							?>
		                
		                </div>
	                
					</div>
					
					<div class="folha1"></div>
					<div class="folha2"></div>
					<div class="folha3"></div>
					
					<small>
						Funcionamento: Domingo a Quinta-feira, de 11h a meia-noite. <br/>
						Sexta e Sábado de 11h a 1h.
					</small>
					
				</div>
			
			</div>
			
		</section>
	
	<?php endwhile; ?>

<?php get_footer(); ?>