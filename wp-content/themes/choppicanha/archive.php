<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

	<?php include("page-templates/menu.php"); ?>

	<section id="novidades">
	
		<div class="row">

			<?php if ( have_posts() ) : ?>

			<div class="title">

				<span></span>
			
				<h1>
					<?php
						if ( is_day() ) :
							printf( __( 'Daily Archives: %s', 'twentyfourteen' ), get_the_date() );

						elseif ( is_month() ) :
							printf( __( 'Monthly Archives: %s', 'twentyfourteen' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'twentyfourteen' ) ) );

						elseif ( is_year() ) :
							printf( __( 'Yearly Archives: %s', 'twentyfourteen' ), get_the_date( _x( 'Y', 'yearly archives date format', 'twentyfourteen' ) ) );

						else :
							_e( 'Archives', 'twentyfourteen' );

						endif;
					?>
				</h1>
				
			</div>
			
			<div class="large-9 medium-9 small-12 columns">

				<?php while ( have_posts() ) : the_post(); ?>
	
                    <div class="item post">

                        <div class="head">
                        
                        	<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>

                            <div class="date">
                                <span>Postado em <?php echo get_the_date(); ?></span>
                            </div>
                            
                            <div class="clearfix"></div>

                        </div>
                        
                        <div class="image-box">
                        	
                        	<?php echo get_the_post_thumbnail($page->ID, "full"); ?>
                        	
                        </div>

                        <article class="textbox">

                            <?php the_excerpt(); ?>

                        </article>

                    </div>
			
				<?php
				
					endwhile;
					// Previous/next page navigation.
					twentyfourteen_paging_nav();
	
					else :
						// If no content, include the "No posts found" template.
						get_template_part( 'content', 'none' );
	
					endif;
				?>
			
			</div>
			
			<div class="large-3 medium-3 small-12 columns">
				
				<?php get_sidebar(); ?>
			
			</div>
			
		</div>
		
	</section>

<?php get_footer(); ?>
