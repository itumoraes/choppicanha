<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

	<?php include("page-templates/menu.php"); ?>

	<section id="erro">
	
		<div class="row">
			
			<div class="large-12 medium-12 small-12 columns">

				<div class="mensagem">
					<h2>Ocorreu um erro inesperado.</h2>
					<p>A página que você está procurando não existe ou foi retirada pelo administrador. Volte para o início do nosso site <a href="<?php bloginfo("url"); ?>">clicando aqui</a>.</p>
				</div>
			
			</div>
			
		</div>
		
	</section>

<?php get_footer(); ?>
