<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

	<?php include("page-templates/menu.php"); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<section id="novidades">
			
			<div class="row">
			
				<div class="large-12 medium-12 small-12 columns">
				
					<div class="title">
					
						<span></span>
						
						<h1>Novidades</h1>
					
					</div>
				
				</div>
				
				<div class="clearfix"></div>
				
				<div class="large-9 medium-9 small-12 columns">
	
                    <div class="item post">

                        <div class="head">
                        
                        	<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>

                            <div class="date">
                                <span>Postado em <?php echo get_the_date(); ?></span>
                            </div>
                            
                            <div class="clearfix"></div>

                        </div>
                        
                        <div class="image-box">
                        	
                        	<?php echo get_the_post_thumbnail($page->ID, "full"); ?>
                        	
                        </div>

                        <article class="textbox">

                            <?php the_content(); ?>

                        </article>

                    </div>
										
				</div>
				
				<div class="large-3 medium-3 small-12 columns">
				
					<?php get_sidebar(); ?>
				
				</div>
			
			</div>
			
		</section>
	
	<?php endwhile; ?>

<?php get_footer(); ?>
