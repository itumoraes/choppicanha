<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/style.css' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Alegreya:400,700' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Leckerli+One' rel='stylesheet' type='text/css'>
	<link type="image/x-icon" rel="icon" href="<?php echo get_template_directory_uri(); ?>/layout/images/favicon.ico">
    <link type="image/x-icon" rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/layout/images/favicon.ico">
	
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxQ1A9Cx1SdsHdnNp1m40C23CY9SnUUAE&sensor=false"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/lib/jquery-1.9.1.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/lib/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/lib/modernizr.2.6.2.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/assets/fancybox/jquery.fancybox.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/functions/functions.js"></script>
	
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
</head>

<div id="mobile-menu">

	<?php wp_nav_menu( array("menu" => "Menu Responsivo", "container_class" => "mobile-menu") ); ?>

</div>

<body <?php body_class(); ?>>

	<div id="message">Preencha todos os campos corretamente.</div>
