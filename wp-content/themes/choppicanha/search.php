<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

	<?php include("page-templates/menu.php"); ?>

	<section id="novidades">
	
		<div class="row">

			<?php if ( have_posts() ) : ?>

			<div class="title">

				<span></span>
			
				<h1><?php printf( __( 'Search Results for: %s', 'twentyfourteen' ), get_search_query() ); ?></h1>

				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
				
			</div>
			
			<div class="large-9 medium-9 small-12 columns">

				<?php while ( have_posts() ) : the_post(); ?>
	
                    <div class="item post">

                        <div class="head">
                        
                        	<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>

                            <div class="date">
                                <span>Postado em <?php echo get_the_date(); ?></span>
                            </div>
                            
                            <div class="clearfix"></div>

                        </div>
                        
                        <div class="image-box">
                        	
                        	<?php echo get_the_post_thumbnail($page->ID, "full"); ?>
                        	
                        </div>

                        <article class="textbox">

                            <?php the_excerpt(); ?>

                        </article>

                    </div>
			
				<?php
				
					endwhile;
					// Previous/next page navigation.
					twentyfourteen_paging_nav();
	
					else :
						// If no content, include the "No posts found" template.
						get_template_part( 'content', 'none' );
	
					endif;
				?>
			
			</div>
			
			<div class="large-3 medium-3 small-12 columns">
				
				<?php get_sidebar(); ?>
			
			</div>
			
		</div>
		
	</section>

<?php get_footer(); ?>
