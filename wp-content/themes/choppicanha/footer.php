	<footer>
		
		<div class="row">
		
			<div class="large-3 medium-6 small-12 columns">
			
				<img id="negative-logo" src="<?php bloginfo("template_url"); ?>/layout/images/logo-choppicanha-negativa.png" alt="Logo Choppicanha Negativa"/>
				
				<small>Desde 1984</small>
			
			</div>
			
			<div class="large-3 medium-6 small-12 columns">
			
				<h5>Localização</h5>
				
				<p class="localizacao">
					Avenida Jovita Feitosa, 191 <br/>
					Parquelândia <br/>
					Tel: (85) 3452-9199
				</p>
				
				<p class="localizacao">
					Rua Benjamin Barroso, 745 <br/>
					Monte Castelo <br/>
					Tel: (85) 3452-9199
				</p>
			
			</div>
			
			<div class="large-3 medium-6 small-12 columns">
			
				<h5>Newsletter</h5>
				
				<p>Assine nossa newsletter, receba sempre as últimas novidades sobre nossos deliciosos pratos e fique por dentro de tudo.</p>
	
				<?php echo do_shortcode("[vfb id='2']"); ?>
			
			</div>
			
			<div class="large-3 medium-6 small-12 columns">
			
				<h5>Redes Sociais</h5>
				
				<p>Conheça nossas principais redes sociais e fique por dentro do Choppicanha.</p>
			
				<div class="social-container">
					
					<a href="https://www.facebook.com/ChoppicanhaRestaurante" target="_blank" class="face">Facebook</a>
					
					<a href="http://www.tripadvisor.com/Restaurant_Review-g303293-d5328723-Reviews-Choppicanha-Fortaleza_State_of_Ceara.html" target="_blank" class="trip">Tripadvisor</a>
					
				</div>
			
			</div>
		
		</div>
	
	</footer>
	
</body>
</html>