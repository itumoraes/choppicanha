<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'choppicanha');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '30seconds');

/** nome do host do MySQL */
define('DB_HOST', 'localhost:8888');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'I@8YC/|nRp;n9d+!r$SO(>6x_jlg8!q*{3-]@&j!yF=q3[A4 <Dg7/)-6[S`w]rz');
define('SECURE_AUTH_KEY',  'tws?~m`g<-6*K(lGC#o`SV}$IOx9FUdzcdN>XzNsXusVJ!@{j BZcV0qpg1def~<');
define('LOGGED_IN_KEY',    'M|l5d5s0H(J~$%{VpR1rrod1kHwdtu9b{h2DS.33pM|^0BH8:G$W`|am;n6nE-53');
define('NONCE_KEY',        'xk_~{Alot*.P VVCOs4)#)<[Qx*|CinCXy).C !9jNXDuRjxnP2BbD}qLDN?31mR');
define('AUTH_SALT',        '%!Qzqvd]D-HSw?dht0}&PZ}1$Q&Y-OVtcLpiF]|G?+0LW5_PaUpD$|q*NPuWV1w&');
define('SECURE_AUTH_SALT', '?|W|yf<-WY5,6p-7ava84OARTT;M3|g$^T!LI5Bs}fzuEY3m!R<F@Jv9<n:3c:u(');
define('LOGGED_IN_SALT',   'yBet9DeLHxS2LtoodZ5{#FI:C6+):6%|]}`xB!L!/f:|[CzXhwv^UAy=s36@USp7');
define('NONCE_SALT',       'i&s-(XiK<XQHRH/A#C+`-z)&y/i (O:z~|uSUjl6V-|Ou, %[`(-[zZ2v?o)Nb9g');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_bis_';

/**
 * O idioma localizado do WordPress é o inglês por padrão.
 *
 * Altere esta definição para localizar o WordPress. Um arquivo MO correspondente ao
 * idioma escolhido deve ser instalado em wp-content/languages. Por exemplo, instale
 * pt_BR.mo em wp-content/languages e altere WPLANG para 'pt_BR' para habilitar o suporte
 * ao português do Brasil.
 */
define('WPLANG', 'pt_BR');

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
